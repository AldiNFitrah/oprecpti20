from django.shortcuts import render
from rest_framework import viewsets, filters
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import ItemSerializer
from .models import Item
# Create your views here.

class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all().order_by('id')
    serializer_class = ItemSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend,]
    filterset_fields = {
        'price' : ['gte', 'lte'],
        'quantity' : ['gte', 'lte'],
    }
    search_fields = ['name']
    ordering_fields = '__all__'