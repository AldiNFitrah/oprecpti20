from django.db import models
from django.core.validators import *
# Create your models here.

class Item(models.Model) :
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=100, decimal_places=2, validators=[MinValueValidator(0.01)])
    quantity = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(2147483647)])