from rest_framework import serializers
from decimal import Decimal
from .models import Item


class ItemSerializer(serializers.HyperlinkedModelSerializer):

    subtotal = serializers.SerializerMethodField()
    def get_subtotal(self, obj) :
        return f"{obj.price * obj.quantity:.2f}"
    
    class Meta:
        model = Item
        fields = ('id', 'url', 'name', 'price', 'quantity', 'subtotal')